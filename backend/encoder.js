const express = require('express');

const router = express.Router();

const Vigenere = require('caesar-salad').Vigenere;

router.post('/encode', (req, res) => {

    const encode = Vigenere.Cipher(req.body.password).crypt(req.body.message);

    res.send({"encoded": encode})

});

router.post('/decode', (req, res) => {
    const decoder = Vigenere.Decipher(req.body.password).crypt(req.body.message);
    res.send({"decoded": decoder});
});

module.exports = router;
