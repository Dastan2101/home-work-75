const express = require('express');
const cors = require('cors');

const encoder = require('./encoder');

const app = express();

const port = 8000;

app.use(express.json());
app.use(cors());

app.use('/', encoder);

app.listen(port);