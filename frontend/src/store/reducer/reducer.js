import {CHANGE_TEXT, FETCH_DECODE_SUCCESS, FETCH_ENCODE_SUCCESS} from "../actions/actions";

const initialState = {
    encodedMessage : '',
    decodedMessage: '',
    password: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_TEXT:
            return {
                ...state,
                [action.input.type] : action.input.value
            };
        case FETCH_ENCODE_SUCCESS:
            return {...state, decodedMessage: action.code.encoded};

        case FETCH_DECODE_SUCCESS:
            return {...state, encodedMessage: action.code.decoded};
        default:
            return state;
    }
};

export default reducer;