import axios from '../../axios-encoder';

export const FETCH_ENCODE_SUCCESS = 'FETCH_ENCODE_SUCCESS';

export const FETCH_DECODE_SUCCESS = 'FETCH_DECODE_SUCCESS';

export const CHANGE_TEXT = 'CHANGE_TEXT';


export const initEncode = (code) => ({type: FETCH_ENCODE_SUCCESS, code});

export const initDecode = (code) => ({type: FETCH_DECODE_SUCCESS, code});

export const inputHandler = input => ({type: CHANGE_TEXT, input});
export const createEncode = code => {
    return dispatch => {
        return axios.post('/encode', code).then(
            response => {
                dispatch(initEncode(response.data))
            }
        )
    }
};


export const createDecode = code => {
    return dispatch => {
        return axios.post('/decode', code).then(
            response => {
                dispatch(initDecode(response.data))
            }
        )
    }
};
