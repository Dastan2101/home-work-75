import React, {Component} from 'react';
import './App.css';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {createDecode, createEncode, inputHandler} from "../store/actions/actions";
import {connect} from "react-redux";

class App extends Component {

    inputChangeHandler = event => {
        const input = {
            type: event.target.name,
            value: event.target.value
        };
        this.props.inputHandler(input)
    };

    sendEncode = () => {

        if (this.props.state.password !== '') {
            const encode = {
                password: this.props.state.password,
                message: this.props.state.encodedMessage
            };

            this.props.createEncode(encode);
        }


    };

    sendDecode = () => {
        if (this.props.state.password !== '' || this.props.state.decodedMessage !== '') {
            const decode = {
                password: this.props.state.password,
                message: this.props.state.decodedMessage
            };
            this.props.createDecode(decode);
        }

    };

    render() {

        return (
            <div className="App"
                 style={{width: '800px', border: '1px solid grey', margin: '50px auto', padding: '20px'}}>
                <Form>
                    <FormGroup row>
                        <Label sm={2} for="encode">Decoded message</Label>
                        <Col sm={8}>
                            <Input
                                type="textarea"
                                name="encodedMessage"
                                style={{height: '150px', resize: 'none'}}
                                value={this.props.state.encodedMessage}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                    </FormGroup>
                    <FormGroup row>
                        <Label sm={2} for="password">Password</Label>
                        <Col sm={3}>
                            <Input
                                type="text" min="0"
                                name="password"
                                value={this.props.state.password}
                                onChange={this.inputChangeHandler}
                            />
                        </Col>
                        <Button style={{margin: '0 5px'}} onClick={this.sendDecode}>&#8593;</Button>
                        <Button tyle={{margin: '0 5px'}} onClick={this.sendEncode}>&#8595;</Button>

                    </FormGroup>

                    <FormGroup row>
                        <Label sm={2} for="decode">Encoded message</Label>
                        <Col sm={8}>
                            <Input
                                type="textarea"
                                name="decodedMessage"
                                style={{height: '150px', resize: 'none'}}
                                value={this.props.state.decodedMessage}
                                onChange={this.inputChangeHandler}

                            />

                        </Col>
                    </FormGroup>
                </Form>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    state: state,
});

const mapDispatchToProps = dispatch => ({
    inputHandler: input => dispatch(inputHandler(input)),
    createEncode: (code) => dispatch(createEncode(code)),
    createDecode: code => dispatch(createDecode(code))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
